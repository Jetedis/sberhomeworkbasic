package homeworkProfile.hw2.task5_Dop;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] words = new String[] {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};

        String[] result = mostFrequency(words, 4);
        System.out.println(Arrays.toString(result));
    }

    private static String[] mostFrequency(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();

        Arrays.stream(words).forEach(word -> {
            if (map.containsKey(word)) {
                int count = map.get(word);
                map.put(word, count + 1);
            } else {
                map.put(word, 1);
            }
        });

        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(map.entrySet());
        entryList.sort((e1, e2) -> {
            if (e1.getValue().equals(e2.getValue())) {
                return (e2.getKey().compareTo(e1.getKey()));
            }
            return e2.getValue().compareTo(e1.getValue());
        });

        List<String> list = entryList.subList(0, k).stream().map(Map.Entry::getKey).toList();

        return  list.toArray(new String[0]);
    }
}

