package homeworkProfile.hw2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Document> documents = new ArrayList<>();
        documents.add(new Document(1256, "doc1", 12));
        documents.add(new Document(1244, "doc2", 15));
        documents.add(new Document(1282, "doc3", 11));
        documents.add(new Document(1230, "doc4", 17));

        Map<Integer, Document> docMap = organizeDocuments(documents);
        Document doc = docMap.get(1282);
        System.out.println(doc);
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document document : documents) {
            map.put(document.getId(), document);
        }
        return map;
    }
}