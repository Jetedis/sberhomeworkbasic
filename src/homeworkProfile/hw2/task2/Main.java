package homeworkProfile.hw2.task2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        String t = scanner.nextLine();

        System.out.println(check (s, t));
        System.out.println(checkRight (s, t));
    }

    private static boolean check (String s, String t) {
        if (s.length() > t.length() || s.length() < t.length()) {
            return false;
        }
        char[] s1 = s.toCharArray();
        char[] t1 = t.toCharArray();
        Arrays.sort(s1);
        Arrays.sort(t1);
        return Arrays.equals(s1, t1);
    }
    private static boolean checkRight (String s, String t) {
        if (s.length() > t.length() || s.length() < t.length()) {
            return false;
        }
        char[] s1 = s.toCharArray();
        char[] t1 = t.toCharArray();
        for (int i = 0; i < s.length() ; i++) {
            if (s1 [i] == t1 [s.length()-i-1]) {
                return true;
            }
        }
        return false;
    }
}
