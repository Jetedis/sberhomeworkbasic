package homeworkProfile.hw2.task3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Set<Integer> set1 = new HashSet<>();
        set1.add(10);
        set1.add(20);
        set1.add(30);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(10);
        set2.add(20);
        set2.add(40);

        PowerfulSet powerfulSet = new PowerfulSet();
        System.out.println(powerfulSet.intersection(set1, set2));
        System.out.println(powerfulSet.union(set1, set2));
        System.out.println(powerfulSet.relativeComplement(set1, set2));
    }
}
