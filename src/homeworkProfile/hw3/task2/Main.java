package homeworkProfile.hw3.task2;

import homeworkProfile.hw3.task1.IsLike;

public class Main {
    public static void main(String[] args) {
        check(TestMethodOne.class);
        check(TestMethodTwo.class);
    }

    private static void check(Class<?> clazz) {
        if (clazz.isAnnotationPresent (IsLike.class)) {
            IsLike ann = clazz.getAnnotation(IsLike.class);
            boolean isLike = ann.value();
            System.out.println(isLike);
        }
        else {
            System.out.println("Annotation is not found");
        }
    }
}


