package homeworkProfile.hw3.task5_dop;

public class Main {
    public static void main(String[] args) {
        String s1 = "(()()())";
        String s2 = ")(";
        String s3 = "(()";
        String s4 = "((()))";

        System.out.println("Строка s1 = " + check(s1));
        System.out.println("Строка s2 = " + check(s2));
        System.out.println("Строка s3 = " + check(s3));
        System.out.println("Строка s4 = " + check(s4));
    }
    static boolean check (String s) {
        char[] arr = s.toCharArray();
        int count1 = 0;
        int count2 = 0;
        boolean result = false;
        if (arr.length %2 == 0) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == '(') {
                    count1++;
                }
                if (arr [i] == ')') {
                    count2 ++;
                    if (count1 >= count2) {
                        result = true;
                    }
                } else {
                    result = false;
                }
            }
        }
        return result;
        }
}
