package homeworkProfile.hw3.task6_dop;

public class Main {
    public static void main(String[] args) {
        String s1 = "{()[]()}";
        String s2 = "{)(}";
        String s3 = "[}";
        String s4 = "[{(){}}][()]{}";

        System.out.println(check(s1));
        System.out.println(check(s2));
        System.out.println(check(s3));
        System.out.println(check(s4));
    }
    static boolean check (String s) {
        boolean result;
        while (true) {
            String str = s.replaceAll("(\\(\\)|\\{\\}|\\[\\])", "");
            if (str.isEmpty()) {
                result = true;
                break;
            } else {
                if (s.equals(str)) {
                    result = false;
                    break;
                } else {
                    s = str;
                }
            }
        }
        return result;
    }
}
