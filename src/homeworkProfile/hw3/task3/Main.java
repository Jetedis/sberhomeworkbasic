package homeworkProfile.hw3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Class<APrinter> clazz = APrinter.class;
        try {
            Method print = clazz.getMethod("print", int.class);
            APrinter instance = clazz.getDeclaredConstructor().newInstance();
            print.invoke(instance, 3);
        } catch (NoSuchMethodException e) {
            System.out.println("Метод отсутствует");
        } catch (SecurityException e) {
            System.out.println("Ошибка безопасности");
        } catch (IllegalAccessException e) {
            System.out.println("Ошибка модификатора доступа");
        } catch (IllegalArgumentException e) {
            System.out.println("Некорректный аргумент");
        } catch (InvocationTargetException e) {
            System.out.println("Выбрасывание исключения");
        } catch (InstantiationException e) {
            System.out.println("Ошибка при создании экземпляра");
        }
    }
}
