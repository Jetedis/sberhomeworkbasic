package homeworkProfile.hw1.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int n = inputN();
        System.out.println("Успешный ввод!");
    }
    private static int inputN() throws IllegalArgumentException{
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 0 && n > 100) {
            throw new IllegalArgumentException ("Неверный ввод");
        }
        return n;
    }
}
