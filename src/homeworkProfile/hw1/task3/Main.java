package homeworkProfile.hw1.task3;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        try {
            readAndWrite();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void readAndWrite() throws IOException {

        File inputFile = new File("src\\homeworkProfile\\hw1\\task3\\input.txt");
        if (!inputFile.canRead()) {
            throw new FileNotFoundException("Файл недоступен");
        }
        if (inputFile.length() == 0) {
            throw  new FileNotFoundException ("Файл пуст");
        }

        File outputFile = new File("src\\homeworkProfile\\hw1\\task3\\output.txt");
        if (!outputFile.canWrite()) {
            throw new FileNotFoundException("Файл недоступен");
        }


        try (
                Reader reader = new FileReader(inputFile);
                Writer writer = new FileWriter(outputFile);
        ) {
            StringBuilder str = new StringBuilder();
            int i;
            while ((i = reader.read()) != -1) {
                char el = (char) i;
                if (String.valueOf(el).matches("[a-z]")) {
                    str.append(Character.toUpperCase(el));
                } else {
                    str.append(el);
                }
            }
            writer.write(str.toString());
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
}
