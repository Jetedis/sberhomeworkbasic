package homeworkProfile.hw1.task4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) {
        if (even(n)) {
            this.n = n;
        }
        else throw new RuntimeException("Введено нечётное число");
    }
    public boolean even (int n) {
            return (n % 2 == 0);
    }

    public int getN() {
        return n;
    }
    public void print () {
        System.out.println(getN());
    }

}
