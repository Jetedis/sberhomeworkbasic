package homeworkProfile.hw1.task6;

public class Main {
    public static void main(String[] args) {
        FormValidator name = new FormValidator();
        FormValidator date = new FormValidator();
        FormValidator gender = new FormValidator();
        FormValidator height = new FormValidator();

        name.checkName("Jetedis");
        date.checkBirthdate("11.11.2011");
        gender.checkGender("FEMALE");
        height.checkHeight("171.7");

    }
}
