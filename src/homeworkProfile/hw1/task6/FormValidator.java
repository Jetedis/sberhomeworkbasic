package homeworkProfile.hw1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

public class FormValidator {

    public void checkName(String str) throws IllegalArgumentException {
        if (str.length() > 2 && str.length() < 20 && Character.isUpperCase(str.charAt(0))) {
        } else
            throw new IllegalArgumentException("Некорректно введено имя: Длина имени должна быть от 2 до 20 символов, первая буква заглавная.");
    }

    public void checkBirthdate(String str) throws IllegalArgumentException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.uuuu").withResolverStyle(ResolverStyle.STRICT);

        LocalDate date;
        try {
            date = LocalDate.parse(str, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Некорректный формат даты");
        }

        LocalDate minDate = LocalDate.of(1900, 1, 1);
        LocalDate maxDate = LocalDate.now();
        if (date.isBefore(minDate) || date.isAfter(maxDate)) {
            throw new IllegalArgumentException("Некорректно введена дата рождения: Дата рождения должна быть в промежутке между 01.01.1900 и текущей датой.");
        }
    }

    public void checkGender(String str) throws IllegalArgumentException {
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Некорректно введён гендер");
        }
    }

    public void checkHeight(String str) throws IllegalArgumentException {
        double height = Double.parseDouble(str);
        if (height <= 0) {
            throw new IllegalArgumentException("Некорректно введён рост: Рост должен быть положительным числом");
        }
    }
}