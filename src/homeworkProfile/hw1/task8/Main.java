package homeworkProfile.hw1.task8;

import java.util.Scanner;

/*
На вход подается число n, массив целых чисел отсортированных по
возрастанию длины n и число p. Необходимо найти индекс элемента массива
равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
вывести -1.
Решить задачу за логарифмическую сложность.

 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int [] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr [i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if (arr [i] == p) {
                System.out.println(i);
            }
        }
    }
}
