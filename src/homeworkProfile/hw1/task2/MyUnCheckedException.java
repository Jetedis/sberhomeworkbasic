package homeworkProfile.hw1.task2;

public class MyUnCheckedException extends IndexOutOfBoundsException{
    protected MyUnCheckedException() {
        super("Обращение к элементу массива за его пределами");
    }

}
