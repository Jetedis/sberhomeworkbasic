package homeworkProfile.hw1.task1;


public class Main {
    public static void main(String[] args) {
        Tea tea = new Tea();

        try {
            tea.addBagTea();
            tea.addWater();
            tea.drinkTea();
        } catch (MyCheckedException e) {
            e.printStackTrace();

        }
    }
}
