package homeworkProfile.hw1.task1;

public class Tea {
    boolean water;
    boolean bagTea;

    public void addBagTea () {
        this.bagTea = true;
        System.out.println("Пакетик чая в чашке");
    }
    public void addWater () {
        this.water = true;
        System.out.println("Чайник вскипел");
    }
    public void drinkTea () throws MyCheckedException {
        System.out.println("Пора сделать перерыв!");
        if (bagTea && water) {
            System.out.println("Чай заварен, можно пить");
        } else {
            System.out.println("Чай не готов");
        }
    }
}
