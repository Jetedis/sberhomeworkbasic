package hw2_1;

import java.util.Scanner;

public class task10 {
    public static void main(String[] args) {
        play();
    }

    private static void play () {
        final int max = 1000;
        int number = (int) (Math.random() * (max + 1));

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Угадайте число: ");
            int digit = scanner.nextInt();

            if (digit < 0) {
                break;
            }
            if (digit == number) {
                System.out.println("Победа!");
                break;
            }
            if (digit > number) {
                System.out.println("Это число больше загаданного.");
            }
            if (digit < number) {
                System.out.println("Это число меньше загаданного.");
            }
        }
    }
}