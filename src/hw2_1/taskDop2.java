package hw2_1;

import java.util.Scanner;

public class taskDop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String mail = scanner.nextLine();

        boolean hasStones = mail.matches(".*камни!.*");
        boolean hasBox = mail.matches(".*запрещенная продукция.*");

        if (hasStones && hasBox) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (hasStones) {
            System.out.println("камни в посылке");
        } else if (hasBox) {
            System.out.println("в посылке запрещенная продукция");
        } else {
            System.out.println("все ок");
        }
    }
}
