package hw3_1.task08;

public class Main {
    public static void main(String[] args) {

        Atm atm = new Atm(71, 0.012);

        double usd1 = 120;
        double rub1 = atm.convertDollarsRoubles(usd1);
        System.out.println(usd1 +" = "+ rub1);

        double rub2 = 400;
        double usd2 = atm.convertRoublesDollars(rub2);
        System.out.println(rub2 +" = "+ usd2);


        int count = atm.getCount();
        System.out.println(count);
    }
}
