package hw3_1.task08;

public class Atm {
    private static int count = 0;

    private double roublesDollar;

    private double dollarsRouble;

    public Atm(double roublesDollar, double dollarsRouble) {
        this.roublesDollar = roublesDollar;
        this.dollarsRouble = dollarsRouble;
        count++;
    }

    public static int getCount() {
        return count;
    }

    public double convertDollarsRoubles(double dollars) {
        return dollars * roublesDollar;
    }

    public double convertRoublesDollars(double roubles) {
        return roubles * dollarsRouble;
    }

}
