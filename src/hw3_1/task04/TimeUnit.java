package hw3_1.task04;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUnit {
    private int hour;
    private int minute;
    private int sec;

    public TimeUnit(int hour, int minute, int sec) {
        setHour(hour);
        setMinute(minute);
        setSec(sec);
    }

    public TimeUnit (int hour, int minute) {
        setHour(hour);
        setMinute(minute);
        this.sec = 0;
    }
    public  TimeUnit (int hour) {
        setHour(hour);
        this.minute = 0;
        this.sec = 0;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public void timeLocal () {
        LocalTime time = LocalTime.of(hour, minute, sec);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern ("HH:mm:ss");
        System.out.println(time.format(formatter));
    }

    public void timiLocal12 () {
        LocalTime time = LocalTime.of(hour, minute, sec);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern ("hh:mm:ss a");
        System.out.println(time.format(formatter));
    }

    public void addTime (int hour, int minute, int sec) {
        LocalTime localTime = LocalTime.of(this.hour, this.minute, this.sec);
        localTime = localTime.plusHours(hour);
        localTime = localTime.plusMinutes(minute);
        localTime = localTime.plusSeconds(sec);
        this.hour = localTime.getHour();
        this.minute = localTime.getMinute();
        this.sec = localTime.getSecond();
    }
}
