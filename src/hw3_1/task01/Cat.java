package hw3_1.task01;

import java.util.Random;

public class Cat {
    public void status () {
        Random random = new Random();
        int n = random.nextInt(3);
        if (n == 0) {
            sleep();
        }
        if (n == 1) {
            meow();
        }
        if (n == 2) {
            eat();
        }
    }

    public void sleep () {
        System.out.println("Sleep");
    }
    public void meow () {
        System.out.println("Meow");
    }
    public void eat () {
        System.out.println("Eat");
    }
}
