package hw3_1.task06;

public class Main {
    public static void main(String[] args) {
        AmazingString strokaString = new AmazingString("This life is beautiful");
        AmazingString strokaChar = new AmazingString(new char[] {'B', 'i',' ', 'l', 'd', 'i', 'n', 'g'});

        System.out.println (strokaString.charAt(2));
        System.out.println (strokaString.length());
        strokaString.enter();
        System.out.println();
        System.out.println (strokaString.have("life"));
        System.out.println(strokaString.space());
        System.out.println(strokaString.reverse());


        System.out.println (strokaChar.charAt(4));
        System.out.println (strokaChar.length());
        strokaChar.enter();
        System.out.println();
        System.out.println (strokaChar.have(new char[]{'d'}));
        System.out.println(strokaChar.space());
        System.out.println(strokaChar.reverse());


    }
}
