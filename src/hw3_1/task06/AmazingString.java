package hw3_1.task06;

public class AmazingString {
    private char[] array;

    public AmazingString(char[] array) {
        this.array = array;
    }

    public AmazingString(String str) {
        this.array = str.toCharArray();
    }

    public char charAt(int index) {
        return array[index];
    }

    public int length() {
        return array.length;
    }

    public void enter() {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("" + array[i]);
            }
        }

    public boolean have (String s) {
        for (int i = 0; i < array.length - (s.length() - 1); i++) {
            String str = "";
            for (int j = 0; j < s.length(); j++) {
                str += array[i + j];
            }
            if (str.equals(s)) {
                return true;
            }
        }
        return false;
    }


    public boolean have (char[] s) {
        for (int i = 0; i < array.length - (s.length - 1); i++) {
            boolean flag = true;
            for (int j = 0; j < s.length; j++) {
                if (array[i + j] != s[j]) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        String str = "";
        for (char element : array) {
            str += element;
        }
        return str;
    }

    public String space() {
        for (int i = 0; i < array.length; i++) {
            if (!Character.isWhitespace(array[i])) {
                char[] newArray = new char[array.length - i];
                System.arraycopy(array, i, newArray, 0, array.length - i);
                array = newArray;
                break;
            }
        }
        return toString();
    }

    public String reverse() {
        char[] newArray = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[array.length - 1 - i];
        }
        array = newArray;
        return toString();
    }

}
