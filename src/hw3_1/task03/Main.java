package hw3_1.task03;

import hw3_1.task02.Student;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {

        Student[] students = new Student[] {
                new Student("Иван", "Иванов", new int [] {10, 11, 12, 13, 14}),
                new Student("Пётр", "Петров", new int [] {15, 16, 17, 18, 19}),
                new Student("Фёдор", "Фёдоров", new int [] {11, 14, 16, 18, 10}),
                new Student("Андрей", "Андреев", new int [] {18, 14, 12, 19, 15}),
        };

        StudentService studentService = new StudentService();
        Student bestStudent = studentService.bestStudent(students);
        System.out.println("Лучший студент: " + bestStudent.toString());
        System.out.println();

        Student[] sortStud = studentService.sort(students);
        for (Student student : sortStud) {
            System.out.println(student.toString());
        }

    }
}
