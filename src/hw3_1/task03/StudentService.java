package hw3_1.task03;

import hw3_1.task02.Student;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {
    private int bestStudent;

    public Student bestStudent(Student[] studentsArr)  {
        Student bestStudent = studentsArr [0];
        for (int i = 1; i < studentsArr.length; i++) {
            if (studentsArr[i].getAverageGradeSS() > bestStudent.getAverageGradeSS()) {
                bestStudent = studentsArr[i];
            }
        }
        return bestStudent;
    }

    public Student[] sort(Student[] students) {
        Arrays.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                return s1.getSurname().length() - s2.getSurname().length();
            }
        });
        return students;

    }
}
