package hw3_1.task02;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Student student1 = new Student("Ivan", "Ivanov");

        System.out.println(student1.toString());
        int [] grades = new int[] {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
        student1.setGrades(grades);
        System.out.println(Arrays.toString(student1.getGrades()));

        int gradesD = 20;
        student1.setGradesD(gradesD);
        System.out.println("Добавить оценку: "+student1.getGradesD());

        student1.gradesAdd (grades, gradesD);
        System.out.println(Arrays.toString(student1.getGradesAdd()));

        double averageGrade = student1.getAverageGrade();
        System.out.println("Средний балл студента: " + averageGrade);
    }
}
