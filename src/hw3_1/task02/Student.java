package hw3_1.task02;

public class Student {
    private String name;
    private String surname;
    private int gradesD;
    private int [] grades = new int [10];
    private int [] arr = new int [10];
    private int count;


    public Student (String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.setGrades(grades);
    }
    public Student (String name, String surname, int [] grades) {
        this.name = name;
        this.surname = surname;
        this.setGrades(grades);;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public int getGradesD() {
        return gradesD;
    }

    public void setGrades(int [] grades) {
        if (grades.length > 10) {
            System.arraycopy(grades, 0, this.grades, 0, 10);
            count = 10;
        } else {
            int[] ArrGrades = new int[10];
            System.arraycopy(grades, 0, ArrGrades, 0, grades.length);
            this.grades = ArrGrades;
            count = grades.length;
        }
    }

    public int [] gradesAdd (int [] grades, int gradesD) {
        if (grades.length >= 10) {
            System.arraycopy(grades, 1, arr, 0, 9);
            arr [9] = gradesD;
            return arr;
        } else {
            System.arraycopy(grades, 0, arr, 0, 9);
            arr [grades.length+1] = gradesD;
            count += 1;
            return arr;
        }
    }

    public int[] getGradesAdd() {
        return arr;
    }

    public void setGradesD(int gradesD) {
        this.gradesD = gradesD;
    }

    public double getAverageGradeSS() {
        if (count == 0) {
            return 0;
        }
        int sum = 0;
        for (int i = 0; i < count; i++) {
            sum += grades[i];
        }
        return (double) sum / count;
    }
    public double getAverageGrade() {
        if (count == 0) {
            return 0;
        }
        int sum = 0;
        for (int i = 0; i < count; i++) {
            sum += arr[i];
        }
        return (double) sum / count;
    }

    @Override
    public String toString() {
        return "Студент {" +
                " имя = '" + name + '\'' +
                ", фамилия = '" + surname + '\'' +
                '}';
    }
}
