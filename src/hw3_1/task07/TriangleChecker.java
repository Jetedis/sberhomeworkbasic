package hw3_1.task07;

public class TriangleChecker {

    private TriangleChecker () {}

    public static boolean goodTriangle (double s1, double s2, double s3) {
        return (s1 + s2 + s3) > 2 * Math.max(s1, Math.max(s2, s3));
    }

}
