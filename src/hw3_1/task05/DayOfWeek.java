package hw3_1.task05;

public class DayOfWeek {
    private byte numberDay;
    private String day;

    public DayOfWeek (byte numberDay, String day) {
        this.numberDay = numberDay;
        this.day = day;
    }

    public byte getNumberDay() {
        return numberDay;
    }

    public String getDay() {
        return day;
    }
}
