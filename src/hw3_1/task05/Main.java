package hw3_1.task05;

public class Main {
    public static void main(String[] args) {
        DayOfWeek[] dayOfWeek = new DayOfWeek[]{
                new DayOfWeek((byte) 1, " Monday"),
                new DayOfWeek((byte) 2, " Tuesday"),
                new DayOfWeek((byte) 3, " Wednesday"),
                new DayOfWeek((byte) 4, " Thursday"),
                new DayOfWeek((byte) 5, " Friday"),
                new DayOfWeek((byte) 6, " Saturday"),
                new DayOfWeek((byte) 7, " Sunday"),
        };

        for (DayOfWeek element : dayOfWeek) {
            System.out.println(String.format("%d %s", element.getNumberDay(), element.getDay()));
        }
    }
}
