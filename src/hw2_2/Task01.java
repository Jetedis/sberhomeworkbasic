package hw2_2;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int [][] arr = new int [n][m];
        for (int i = 0; i < n; i++) {  //строка
            for (int j = 0; j < m; j++) { //столбец
                arr [i][j] = scanner.nextInt();
            }
        }

        int [] result = new int [n];
        for (int i = 0; i < n; i++) {
            int min = arr [i][0];
            for (int j = 0; j < m; j++) {
                if (min > arr[i][j]) {
                    min = arr[i][j];
                }
            }
            result [i] = min;

        }
        for (int k = 0; k < result.length; k++) {
            System.out.print(result[k] + " ");
        }
    }
}
