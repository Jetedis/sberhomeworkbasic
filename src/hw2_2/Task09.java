package hw2_2;

import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        backspace(n);
    }
    private static void backspace(int n) {
        String str = n + "";
        backspace(str);
    }
    private static void backspace(String str) {
        if (str.length() == 1) {
            System.out.print(str);
            return;
        }

        String i = str.substring(0, 1);
        System.out.print(i + " ");

        String m = str.substring(1);
        backspace(m);
    }
}
