package hw2_2;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] arr = new int[n][n];

        for ( int i = 0; i < n; i++) {
            for ( int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();

        int k = 0;
        int l = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == p) {
                    k = i;
                    l = j;
                }
            }
        }

        int [] [] array = new int[n-1][n-1];

        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-1; j++) {
                array[i][j] = arr[i < k ? i : i + 1][j < l ? j : j + 1];
            }
        }
        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-1; j++) {
                System.out.print(array[i][j]);
                if (j < n - 2) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
