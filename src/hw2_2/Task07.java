package hw2_2;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String [] name = new String[n];
        for (int i = 0; i < n; i++) {
            name [i] = scanner.next();
        }
        String [] nickname = new String[n];
        for (int i = 0; i < n; i++) {
            nickname[i] = scanner.next();
        }
        int [][] mark = new int [n][3];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                mark [i][j] = scanner.nextInt();
            }
        }
        double [] result = new double[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                result [i] += mark [i][j];
            }
        }
        double [] finalRes = new double [n];
        for (int i = 0; i < n; i++) {
            finalRes [i] = result[i]/3;
        }
        double first = finalRes[0];
        int indexOfFirst = 0;
        for (int i = 1; i < n; i++) {
            if (finalRes[i] > first) {
                first = finalRes[i];
                indexOfFirst = i;
            }
        }
        double second = finalRes[0];
        int indexOfSecond = 0;
        for (int i = 1; i < n; i++) {
            if (finalRes [i] != first) {
                if (finalRes[i] > second) {
                    second = finalRes[i];
                    indexOfSecond = i;
                }
            }
        }
        double third = finalRes[0];
        int indexOfThird = 0;
        for (int i = 1; i < n; i++) {
            if (finalRes [i] != first && finalRes [i] != second) {
                if (finalRes[i] > third) {
                    third = finalRes[i];
                    indexOfThird = i;
                }
            }
        }

        DecimalFormat df = new DecimalFormat("0.0");
        df.setRoundingMode(RoundingMode.DOWN);
        String s1 = df.format(first);
        String s2 = df.format(second);
        String s3 = df.format(third);

        System.out.println(name[indexOfFirst] + ": "+ nickname[indexOfFirst] + ", " + s1);
        System.out.println(name[indexOfSecond] + ": "+ nickname[indexOfSecond] + ", " + s2);
        System.out.println(name[indexOfThird] + ": "+ nickname[indexOfThird] + ", " + s3);
    }
}
