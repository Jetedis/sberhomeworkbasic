package hw2_2;

import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(sum(n));
    }
    public static int sum (int n) {
        if (n / 10 >= 1) {
            int temp = n % 10;
            int result = n / 10;
            return temp + sum(result);
        } else {
            return n;
        }
    }
}
