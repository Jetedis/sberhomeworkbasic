package hw2_2;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] arr = new int[n][n];

        for ( int i = 0; i < n; i++) {
            for ( int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] != arr[n - 1 - j][n - 1 - i]) {
                    flag = false;
                }
            }
        }
        System.out.print(flag);
    }
}
