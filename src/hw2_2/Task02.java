package hw2_2;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] arr = new int[n][n];

        int [] start = new int [2];
        int [] end = new int [2];

        for (int i = 0; i < 2; i++) {
            start [i] = scanner.nextInt();
        }
        for (int i = 0; i < 2; i++) {
            end [i] = scanner.nextInt();
        }

        arr [start[1]][start[0]] = 1;
        arr [start[1]][end[0]] = 1;
        arr [end[1]][start[0]] = 1;
        arr [end[1]][end[0]] = 1;


        for (int i = start[1]; i < end[1]+1; i++) {
            for (int j = start[0]; j < end[0]+1; j++) {
                arr [start[1]][j] = 1;
                arr [end[1]][j] = 1;
                arr [i][start[0]] = 1;
                arr [i][end[0]] = 1;

            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j]);
                if (j<n-1) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
