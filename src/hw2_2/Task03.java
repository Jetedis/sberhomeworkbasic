package hw2_2;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        char[][] arr = new char[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = '0';
            }
        }

        int x0 = scanner.nextInt();
        int y0 = scanner.nextInt();

        arr [y0][x0] = 'K';

        motion(arr, n, x0, y0, -2, -1);
        motion(arr, n, x0, y0, -1, -2);
        motion(arr, n, x0, y0, 1, -2);
        motion(arr, n, x0, y0, 2, -1);
        motion(arr, n, x0, y0, 1, 2);
        motion(arr, n, x0, y0, -1, 2);
        motion(arr, n, x0, y0, -2, 1);
        motion(arr, n, x0, y0, 2, 1);


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j]);
                if (j < n - 1) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void motion(char[][] arr, int n, int x0, int y0, int x1, int y1) {

        if (x0 + x1 < 0 ||
                x0 + x1 > n - 1 ||
                y0 + y1 < 0 ||
                y0 + y1 > n - 1) {
        } else {
            arr[y0 + y1][x0 + x1] = 'X';
        }
    }
}
