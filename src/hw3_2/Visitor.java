package hw3_2;

import java.util.UUID;


public class Visitor {
    private String visitorName;
    private UUID id;

    public Visitor (String visitorName, UUID id) {
        this.visitorName = visitorName;
        this.id = null;
    }
    public String getVisitorName() {
        return visitorName;
    }
    public void setId() {
        this.id = UUID.randomUUID();
    }
    public void setIdNull() {
        this.id = null;
    }
    public UUID getId() {
        return id;
    }
}
