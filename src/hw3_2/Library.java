package hw3_2;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Library {
    private ArrayList<Book> books = new ArrayList<>();
    private ArrayList<Book> booksAuthor = new ArrayList<>();

    public Library() {
    }
    public boolean addBook(Book book) {
        for (Book element : books) {
            if (element.getBookName().equals(book.getBookName())) {
                System.out.println("Книга уже находится в библиотеке");
                return false;
            }
        }
        books.add(book);
        System.out.println("Книга успешно добавлена в библиотеку");
        return true;
    }
    public boolean removeBook(String bookName) {
        if (!books.contains(find(bookName))) {
            System.out.println("Данной книги нет в библиотеке или она одолжена");
            return false;
        } else {
            books.remove(find(bookName));
            System.out.println("Книга успешно удалена из библиотеки");
            return true;
        }
    }
    public Book find(String bookName) {
        for (Book element : books) {
            if (element.getBookName().equals(bookName)) {
                return element;
            }
        }
        return null;
    }
    public void findBookName(String bookName) {
        for (Book element : books) {
            if (element.getBookName().equals(bookName)) {
                System.out.println(element.getBookName());
            }
        }
    }
    public void findBookAuthor(String authorName) {
        for (Book element : books) {
            if (element.getAuthor().equals(authorName)) {
                booksAuthor.add(element);
            }
        }
        for (Book el : booksAuthor) {
            System.out.println(el.getBookName());
        }
    }
    public void lendBook (String bookName, Visitor visitor) {
        if (books.contains(find(bookName))) {
            if (visitor.getId() == null) {
                visitor.setId();
                removeBook(bookName);
            }
            else {
                System.out.println("Прежде чем брать новую книгу, Вам необходимо вернуть одолженную");
            }
        }
        else {
            System.out.println("Такой книги нет, или она одолжена");
        }
    }
    public void returnBook (String bookName, Visitor visitor, UUID id) {
        if (visitor.getId() == id) {
            books.remove(find(bookName));
            visitor.setIdNull();
            System.out.println("Книга успешно возвращена в библиотеку");
        } else {
            System.out.println("Вы не можете вернуть книгу, одолженную другим посетителем");
        }
    }
}




