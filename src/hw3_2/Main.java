package hw3_2;

public class Main {
    public static void main(String[] args) {

        Library library = new Library();
        System.out.println("Добавить книгу в библиотеку:");
        library.addBook(new Book("Горе от ума", "А.С.Грибоедов"));
        library.addBook(new Book("Евгений Онегин", "А.С.Пушкин"));
        library.addBook(new Book("Палата №6", "А.П.Чехов"));
        library.addBook(new Book("Капитанская дочка", "А.С.Пушкин"));
        library.addBook(new Book("Война и мир", "Л.Н.Толстой"));
        library.addBook(new Book("Герой нашего времени", "М.Ю.Лермонтов"));
        library.addBook(new Book("Война и мир", "Л.Н.Толстой"));

        System.out.println("");
        System.out.println("Удалить книгу из библиотеке:");
        library.removeBook("Палата №6");
        library.removeBook("Мёртвые души");

        System.out.println("");
        System.out.println("Найти и вернуть книгу по названию:");
        library.findBookName("Герой нашего времени");
        library.findBookName("Мёртвые души");

        System.out.println("");
        System.out.println("Найти и вернуть список книг по автору:");
        library.findBookAuthor("А.С.Пушкин");

        System.out.println("");
        System.out.println("Одалживание книги:");
        Visitor visitor1 = new Visitor("Елисей", null);
        Visitor visitor2 = new Visitor("Елизаветта", null);
        library.lendBook("Герой нашего времени", visitor1);
        library.lendBook("Герой нашего времени", visitor2);
        library.lendBook("Капитанская дочка", visitor1);

        System.out.println("");
        System.out.println("Возврат книги:");
        library.returnBook("Герой нашего времени", visitor2, visitor1.getId());
        library.returnBook("Герой нашего времени", visitor1, visitor1.getId());
    }
}
