package hw3_3.task04;

public class Dog {
    private String nickname;

    public Dog(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }
}
