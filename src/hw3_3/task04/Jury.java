package hw3_3.task04;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Jury {
    public static double calculateAverageScore(double[] scores) {
        double sum = 0;
        for (double score : scores) {
            sum += score;
        }
        return sum / scores.length;
    }

    private final List<Participant> participants = new ArrayList<>();

    public void addParticipant(Participant participant) {
        participants.add(participant);
    }

    public Participant getParticipant(int index) {
        return participants.get(index);
    }

    public void printWin() {
        List<Participant> sorted = new ArrayList<>(participants);
        sorted.sort(Comparator.comparingDouble(Participant::getAverageScore).reversed());

        int count = 3;
        for (int i = 0; i < count; i += 1) {
            Participant participant = sorted.get(i);
            String name = participant.getName();
            String nickname = participant.getDog().getNickname();
            double averageScore = participant.getAverageScore();

            DecimalFormat df = new DecimalFormat("0.0");
            df.setRoundingMode(RoundingMode.DOWN);
            String formatedAverageScore = df.format(averageScore);

            System.out.println(String.format("%s: %s, %s", name, nickname, formatedAverageScore));
        }
    }
}

