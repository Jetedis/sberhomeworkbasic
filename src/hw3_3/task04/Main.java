package hw3_3.task04;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Jury jury = new Jury();
        int n = scanner.nextInt();


        for (int i = 0; i < n; i += 1) {
            String name = scanner.next();
            Participant participant = new Participant(name);
            jury.addParticipant(participant);
        }

        for (int i = 0; i < n; i += 1) {
            String nickname = scanner.next();
            Dog dog = new Dog(nickname);
            Participant participant = jury.getParticipant(i);
            participant.setDog(dog);
        }


        int m = 3;
        for (int i = 0; i < n; i++) {
            double[] grade = new double[m];
            for (int j = 0; j < m; j++) {
                grade[j] = scanner.nextDouble();
            }
            Participant participant = jury.getParticipant(i);
            double averageScore = Jury.calculateAverageScore(grade);
            participant.setAverageScore(averageScore);
        }


        jury.printWin();


    }
}
