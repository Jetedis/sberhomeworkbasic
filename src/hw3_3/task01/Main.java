package hw3_3.task01;

public class Main {
    public static void main(String[] args) {

        Bat bat = new Bat();
        Dolphin dolphin = new Dolphin();
        Eagle eagle = new Eagle();
        GoldFish goldFish = new GoldFish();

        System.out.println("Летучая мышь");
        bat.wayOfBirth();
        bat.Fly();
        bat.eat();
        bat.sleep();
        System.out.println("");

        System.out.println("Дельфин");
        dolphin.wayOfBirth();
        dolphin.swim();
        dolphin.eat();
        dolphin.sleep();
        System.out.println("");

        System.out.println("Орёл");
        eagle.wayOfBirth();
        eagle.Fly();
        eagle.eat();
        eagle.sleep();
        System.out.println("");

        System.out.println("Золотая рыбка");
        goldFish.wayOfBirth();
        goldFish.swim();
        goldFish.eat();
        goldFish.sleep();
        System.out.println("");
    }
}
