package hw3_3.task01;

public class Dolphin extends Animal implements Swimming {

    @Override
    public void wayOfBirth() {
        System.out.println("Млекопитающее");
    }

    @Override
    public void swim() {
        System.out.println("Плавает быстро");
    }
}