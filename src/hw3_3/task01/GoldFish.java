package hw3_3.task01;

public class GoldFish extends Animal implements Swimming {

    @Override
    public void wayOfBirth() {
        System.out.println("Рыба");
    }

    @Override
    public void swim() {
        System.out.println("Плавает медленно");
    }
}