package hw3_3.task01;

public abstract class Animal {
    protected Animal() {}

    public abstract void wayOfBirth ();

    public void eat () {
        System.out.println("Ест");
    }
    public void sleep () {
        System.out.println("Спит");
    }
}
