package hw3_3.task02;

public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        Table table = new Table();
        Stool stool = new Stool();

        System.out.println(bestCarpenterEver.toFix(table));
        System.out.println(bestCarpenterEver.toFix(stool));
    }
}
