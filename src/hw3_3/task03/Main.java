package hw3_3.task03;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        ArrayList <ArrayList<Integer>> arrayList1 = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            ArrayList<Integer> arrayList2 = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                arrayList2.add (i + j);
            }
            arrayList1.add(arrayList2);
        }
        for (ArrayList<Integer> i : arrayList1) {
            for (int element : i) {
                System.out.print(element + " ");
            }
            System.out.print("\n");
        }
    }
}
